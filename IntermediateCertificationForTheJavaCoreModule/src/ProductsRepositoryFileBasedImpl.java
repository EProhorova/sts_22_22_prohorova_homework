import java.io.*;
import java.util.List;
import java.util.function.Function;

public class ProductsRepositoryFileBasedImpl implements ProductsRepostory {

    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<Product, String> productToStringMapper = product -> {
        return product.getId()
                + "|" + product.getTitle()
                + "|" + product.getCost()
                + "|" + product.getQuantityInStock();
    };

    private static final Function<String, Product> stringToProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String title = parts[1];
        Double cost = Double.parseDouble(parts[2]);
        Integer quantityInStock = Integer.parseInt(parts[3]);
        return new Product(id, title, cost, quantityInStock);
    };

    @Override
    public Product findById(Integer id) {

        try (BufferedReader rider = new BufferedReader(new FileReader(fileName))) {
            return rider
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getId().equals(id))
                    .findFirst()
                    .get();

        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        try (BufferedReader rider = new BufferedReader(new FileReader(fileName))) {
            return rider
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getTitle().toLowerCase().contains(title.toLowerCase()))
                    .toList();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public void update(Product product) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            List<Product> productList = reader
                    .lines()
                    .map(stringToProductMapper)
                    .toList();

            Product oldProduct = productList
                    .stream()
                    .filter(productFromFile -> productFromFile.getId().equals(product.getId()))
                    .findFirst()
                    .get();

            Product newProduct = new Product(oldProduct.getId(), product.getTitle(), product.getCost(), product.getQuantityInStock());

            List<Product> products = productList
                    .stream()
                    .map(prod -> {
                        if (prod.getId() == newProduct.getId()) {
                            return newProduct;
                        }
                        return prod;
                    })
                    .toList();
            saveAll(products);
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }

    }

    public void saveAll(List<Product> products) {
        try (FileWriter fileWriter = new FileWriter(fileName, false);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            StringBuilder stringProduct = new StringBuilder();
            for (Product product : products) {
                stringProduct.append(productToStringMapper.apply(product)).append("\n");
            }
            bufferedWriter.write(stringProduct.toString());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }
}
