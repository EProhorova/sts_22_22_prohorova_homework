public class Main {
    public static void main(String[] args) {
        ProductsRepostory product = new ProductsRepositoryFileBasedImpl("products.txt");
        Product apricot = product.findById(1);
        System.out.println(product.findById(1));
        System.out.println(product.findAllByTitleLike("мол"));
        apricot.setCost(300.0);
        product.update(apricot);
        System.out.println(product.findById(1));
    }
}