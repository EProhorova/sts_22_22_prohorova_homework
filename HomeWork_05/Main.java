import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int amount = scanner.nextInt();
        int deposit = scanner.nextInt();

        ATM atm = new ATM(300, 500, 1000);

        System.out.println(ATM.cashWithdrawal(atm, amount));

        System.out.println(ATM.depositMoney(atm, deposit));
    }

}



