import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        //1.
        int myFrom = scanner.nextInt();
        int myTo = scanner.nextInt();
        System.out.println(sumNumbersInterval(myFrom,myTo));

        //2.
        int[] array = {8, 10, 15, 11, 17, 14};
        evenArrayElements(array);

        //3*.
        int [] number  = {1,2,3,4,5};
        int result = toInt(number);
    }

    // 1.Написать функцию, возвращающую сумму чисел в каком-либо интервале,
    // если интервал задан неверно (значение левой границы больше, чем правой) - вернуть-1
    public static int sumNumbersInterval (int from, int to){
        int sum = 0;
        for (int i = from; i < to; i++){
            if (from>=to){
                return -1;
            }
            sum += i;
        }

        return sum;
    }

    //2. Написать процедуру, которая для заданного массива выводит все его четные элементы
    public static void evenArrayElements (int[] array){
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0){
                System.out.print(array[i] + " ");
            }
        }
    }

    //3. Реализовать функциюtoInt(), принимающую на вход число, в виде массива цифр, например число 98723
    // 3адано в виде: int[] number = {9, 8, 7, 2, 3}.
    // Результатом работы функции должностать число, записанное в одну переменную типа int
    public static int toInt(int[] arr){
        int result = 0;

        for (int i =arr.length -1 , n = 0; i >= 0; --i, n++) {
            int pos = (int)Math.pow(10, i);
            result += arr[n] * pos;
        }
        return result;
    }

}