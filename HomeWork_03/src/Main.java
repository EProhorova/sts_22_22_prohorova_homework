import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        int[] array = {3, 10, 2, 1, 8, 7, 5};

        // 1. Считать размер массива
        int length = array.length;
        System.out.println(length);

        //2. Считать элементы массива
        boolean exists = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                exists = true;
                break;
            }
        }
        if (exists) {
            System.out.println("Число в массиве есть!");
        } else {
            System.out.println("Числа в массиве нет!");
        }

        // 3. Колличество локальный минимум в массиве
        int min = array[0];
        int sum = 0;
        for (int i = 0; i < array.length; i++)
        {
            if (array[i] < min)
                min = array[i];
                sum++;
        }
        System.out.println(sum);
    }
}